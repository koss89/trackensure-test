/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.trackensure;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author koss
 */
public class Test2 {

    public Test2() {
    }

    public int test(int[] height) {
        
        //В этом задании в условии говорится об литрах, что наталкивает на мысли объема
        //Но объем там вычеслить невозможно, по сути это площадь прямоугольника.
        
        int maxSquare = 0;
        for (int i = 0; i < height.length; i++) {
            for (int j = 0; j < height.length; j++) {
                int tmpNumber = height[j] > height[i] ? height[i] : height[j];
                int horuzont = i > j ? i - j : j - i;
                if (j != i) {
                    int currentSquare = tmpNumber * (horuzont);
                    maxSquare = maxSquare<currentSquare ? currentSquare : maxSquare;
                }
            }
        }
        return maxSquare;
    }

}
