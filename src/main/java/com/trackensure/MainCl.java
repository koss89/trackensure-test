/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.trackensure;

/**
 *
 * @author koss
 */
public class MainCl {

    public static void main(String[] args) throws Exception {
        System.out.println("----------TEST1--------------");
        Test1 test1 = new Test1();
        for (int i = 1; i <= 10; i++) {
            System.out.println("Number: " + i + " Result:" + test1.test(i));
        }
        System.out.println("----------TEST2--------------");
        Test2 test2 = new Test2();
        int[] data = {1, 8, 6, 2, 5, 4, 8, 3, 7};
        System.out.println(test2.test(data));
    }

}
