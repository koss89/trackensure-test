/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.trackensure;

/**
 *
 * @author koss
 */
public class Test1 {

    public Test1() {
    }

    public int test(int n) throws Exception {

        if (n <= 0) {
            throw new Exception("the number must be greater than 0");
        }
        return calculate(0, 1, n);
    }

    private int calculate(int sum, int iteration, int n) {

        int currentSum = sum + iteration;
        if (currentSum < n && currentSum + iteration + 1 <= n) {
            return calculate(currentSum, iteration + 1, n);
        }
        return iteration;
    }

}
